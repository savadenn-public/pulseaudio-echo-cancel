#!/usr/bin/env bash
set -e
set -u

PULSE_FILE=/etc/pulse/default.pa.d/echo_cancel.pa
TMP_PULSE_FILE=/tmp/echo_cancel.pa
CONFIG_FILE="${HOME}/.pulseaudio/config.env"

mkdir -p $(dirname "${CONFIG_FILE}")

NC='\033[0m'
ZEN_WIDTH=300

function messageOK() {
  echo -e "\033[0;92m✓ $1${NC}"
}

function messageAction() {
  echo -e "\033[0;96m⚑ $1"
}

function messageEnd() {
  echo -e "${NC}"
}

function messageError() {
  echo -e "\033[0;91m✗ $1${NC}"
  zenity --error --text="$1" --width="${ZEN_WIDTH}"
}

function confirm() {
  set +e
  zenity --question --text="${1}" --width="${ZEN_WIDTH}"
  exit_code=$?
  set -e
  if [ "${exit_code}" != "0" ]; then
    messageError "Cancelled by user. Exiting"
    exit 1
  fi
}

function killPulse() {
  messageAction "Killing pulseaudio (restart is automatic)"
  pulseaudio -k
  messageOK "Killed"
}

# Configure pulseaudio
# $1 : input
# $2 : output
function configurePulseAudioTmp() {
  tee "${TMP_PULSE_FILE}" 1>/dev/null <<EOF
.nofail
load-module module-echo-cancel source_name=InputEchoCancel source_properties="device.description='InputEchoCancel'" source_master=${1} sink_name=OutputEchoCancel sink_properties="device.description='OutputEchoCancel'" sink_master=${2} aec_method=webrtc  use_master_format=1 aec_args="high_pass_filter=1 noise_suppression=0 analog_gain_control=0"
set-default-sink OutputEchoCancel
set-default-source InputEchoCancel
.fail
EOF
}


# Manually configure pulseaudio via prompt
function manualConfigure() {

  mapfile -t input_options < <(pactl list short sources | grep input |  awk '{ print (( $7 == "RUNNING" || $7 == "IDLE" ) ? "TRUE" : "FALSE") } {print $2} {print $7}' | tr ';' ' ' )
  messageAction "Select the relevant sound input"
  input=$(
    zenity --list --radiolist \
      --title "Select the relevant sound input" \
      --width="900" \
      --height="300" \
      --separator="|" \
      --column="Select" \
      --column="Input device" \
      --column="Status" \
      "${input_options[@]}"
  )
  
  mapfile -t output_options < <(pactl list short sinks | grep output |  awk '{ print (( $7 == "RUNNING" || $7 == "IDLE" ) ? "TRUE" : "FALSE") } {print $2} {print $7}' | tr ';' ' ' )
  messageAction "Select the relevant sound output"
  output=$(
    zenity --list --radiolist \
      --title "Select the relevant sound output" \
      --width="900" \
      --height="300" \
      --separator="|" \
      --column="Select" \
      --column="Output device" \
      --column="Status" \
      "${output_options[@]}"
  )


  configurePulseAudioTmp "${input}" "${output}"

  if ! cmp "${PULSE_FILE}" "${TMP_PULSE_FILE}"; then
    messageAction "Sudo right is needed to modify ${PULSE_FILE}"
    sudo cp --no-preserve=mode,ownership "${TMP_PULSE_FILE}" "${PULSE_FILE}"
    messageOK "Pulseaudio configuration updated"
    killPulse
  else
    messageOK "${PULSE_FILE} already up to date"
  fi

  messageAction "Saving choices for -a usage"
  tee -a "${CONFIG_FILE}" 1>/dev/null <<EOF
  input="${input}"
  output="${output}"
EOF
  messageOK "Saved"
}

# Verify if selected devices are detected
# $1 : input device
# $2 : output device
function checkDeviceDetection() {
  if pactl list short sources | grep "${1}"; then
    messageOK "${1} is detected"
  else
    messageError "${1} is not detected"
    messageError "Restart script in manual mode to choose, or plug device"
  fi

  if pactl list short sinks | grep "${2}"; then
    messageOK "${2} is detected"
  else
    messageError "${2} is not detected"
    messageError "Restart script in manual mode to choose, or plug device"
  fi
}

# Automatic configuration via latest choices
function autoConfigure() {
  if [[ ! -f "${CONFIG_FILE}" ]]; then
    messageError "Missing ${CONFIG_FILE}, run manually first"
    exit 1
  fi

  # shellcheck source=$HOME/.pulseaudio/config.env
  . "${CONFIG_FILE}"

  checkDeviceDetection "${input}" "${output}"

  # Copy current config
  messageAction "Checking pulseaudio configuration"
  configurePulseAudioTmp "${input}" "${output}"
  if ! cmp "${PULSE_FILE}" "${TMP_PULSE_FILE}"; then
    messageError "Expected configuration differ, please use manual mode"
    diff "${PULSE_FILE}" "${TMP_PULSE_FILE}"
    exit 1
  fi

  killPulse
}

function removeAutoCancel() {
  cp "${PULSE_FILE}" "${TMP_PULSE_FILE}"
  cleanConfiguration "${TMP_PULSE_FILE}"
  messageAction "Sudo right is needed to modify ${PULSE_FILE}"
  sudo cp --no-preserve=mode,ownership "${TMP_PULSE_FILE}" "${PULSE_FILE}"
  messageOK "Pulseaudio configuration updated"
  killPulse
}

function printHelp() {
  messageAction "$(basename "$0") COMMAND"
  echo "  Configures pulseaudio echo cancellation"
  echo ""
  echo "              : Starts manual configuration via prompt"
  echo "  --help  | -h : print this help"
  echo "  --auto  | -a : Check configuration via last profil and restart pulseaudio (no sudo right required if configuration is ok)"
  echo "  --clean | -c : Remove echo cancel configuration"
}

#
# Main script
#
if [ "$EUID" == 0 ]; then
  messageError "Please do not run as root, sudo right will be required when needed"
  exit 1
fi

if [[ $# == 0 ]]; then
  manualConfigure
elif [[ $# -gt 1 ]]; then
  printHelp
elif [[ "$1" == "--help" || "$1" == "-h" ]]; then
  printHelp
elif [[ "$1" == "--auto" || "$1" == "-a" ]]; then
  autoConfigure
elif [[ "$1" == "--clean" || "$1" == "-c" ]]; then
  removeAutoCancel
fi
